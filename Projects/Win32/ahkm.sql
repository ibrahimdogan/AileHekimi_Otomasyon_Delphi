/*
SQLyog Ultimate v12.09 (32 bit)
MySQL - 10.1.13-MariaDB : Database - ahkm
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ahkm` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `ahkm`;

/*Table structure for table `doktor` */

DROP TABLE IF EXISTS `doktor`;

CREATE TABLE `doktor` (
  `tc` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL,
  `adi` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL,
  `soyadi` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL,
  `cep` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL,
  `sifre` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

/*Data for the table `doktor` */

insert  into `doktor`(`tc`,`adi`,`soyadi`,`cep`,`sifre`) values ('3684','ibo','doðan','053','123');

/*Table structure for table `hasta` */

DROP TABLE IF EXISTS `hasta`;

CREATE TABLE `hasta` (
  `tc` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL,
  `adi` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL,
  `soyadi` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL,
  `cinsiyet` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL,
  `il` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL,
  `ilce` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL,
  `mahale` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

/*Data for the table `hasta` */

insert  into `hasta`(`tc`,`adi`,`soyadi`,`cinsiyet`,`il`,`ilce`,`mahale`) values ('3684','ibrahim','doðan','BAY','diyarbakýr','çýnar','benusen?');

/*Table structure for table `islemler` */

DROP TABLE IF EXISTS `islemler`;

CREATE TABLE `islemler` (
  `tc` varchar(15) COLLATE utf8_turkish_ci NOT NULL,
  `islem` varchar(15) COLLATE utf8_turkish_ci NOT NULL,
  `doktor` varchar(15) COLLATE utf8_turkish_ci NOT NULL,
  `zaman` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

/*Data for the table `islemler` */

insert  into `islemler`(`tc`,`islem`,`doktor`,`zaman`) values ('36','Ameliyat','doktor2','0000-00-00 00:00:00'),('36','Ameliyat','doktor2','2017-04-19 21:27:54');

/*Table structure for table `yetkili` */

DROP TABLE IF EXISTS `yetkili`;

CREATE TABLE `yetkili` (
  `kulanici` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL,
  `sifre` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

/*Data for the table `yetkili` */

insert  into `yetkili`(`kulanici`,`sifre`) values ('a1','123'),('a1','123');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
