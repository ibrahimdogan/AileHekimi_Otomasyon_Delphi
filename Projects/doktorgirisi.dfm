object Form7: TForm7
  Left = 0
  Top = 0
  Caption = 'Doktor Giri'#351'i'
  ClientHeight = 330
  ClientWidth = 535
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 94
    Top = 57
    Width = 44
    Height = 16
    Caption = 'Ad'#305'n'#305'z:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 190
    Width = 409
    Height = 132
    DataSource = DataSource1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object BitBtn1: TBitBtn
    Left = 296
    Top = 54
    Width = 75
    Height = 25
    Caption = 'Hasta Listeler'
    TabOrder = 1
    OnClick = BitBtn1Click
  end
  object Edit1: TEdit
    Left = 144
    Top = 56
    Width = 121
    Height = 21
    TabOrder = 2
  end
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Database=ahkm'
      'User_Name=root'
      'Server=localhost'
      'DriverID=MySQL')
    Connected = True
    LoginPrompt = False
    Left = 496
    Top = 72
  end
  object FDQuery1: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select * from islemler where doktor='#39'#'#39)
    Left = 488
    Top = 136
  end
  object DataSource1: TDataSource
    DataSet = FDQuery1
    Left = 496
    Top = 216
  end
  object FDTable1: TFDTable
    Active = True
    Connection = FDConnection1
    UpdateOptions.UpdateTableName = 'ahkm.islemler'
    TableName = 'ahkm.islemler'
    Left = 496
    Top = 280
  end
end
