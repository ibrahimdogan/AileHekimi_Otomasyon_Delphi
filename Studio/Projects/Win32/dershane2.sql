/*
SQLyog Ultimate v12.09 (32 bit)
MySQL - 10.1.13-MariaDB : Database - dershane2
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`dershane2` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_turkish_ci */;

USE `dershane2`;

/*Table structure for table `alinanders` */

DROP TABLE IF EXISTS `alinanders`;

CREATE TABLE `alinanders` (
  `no` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL,
  `ogretmen` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL,
  `ders` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL,
  `tarih` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

/*Data for the table `alinanders` */

insert  into `alinanders`(`no`,`ogretmen`,`ders`,`tarih`) values ('153','faruk','mat','2017-04-18');

/*Table structure for table `ogrenci` */

DROP TABLE IF EXISTS `ogrenci`;

CREATE TABLE `ogrenci` (
  `kulanici` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL,
  `sifre` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL,
  `tc` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL,
  `isim` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL,
  `soyisim` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL,
  `fizik` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL,
  `matematik` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL,
  `türkce` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL,
  `biyoloji` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL,
  `no` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

/*Data for the table `ogrenci` */

insert  into `ogrenci`(`kulanici`,`sifre`,`tc`,`isim`,`soyisim`,`fizik`,`matematik`,`türkce`,`biyoloji`,`no`) values ('a1','123','335','amine','köse','30','60','85','85','152600'),('15260039','ibo','368472','ibrahim','doðan',NULL,NULL,NULL,NULL,'15260039');

/*Table structure for table `ogretmen` */

DROP TABLE IF EXISTS `ogretmen`;

CREATE TABLE `ogretmen` (
  `sifre` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL,
  `kulanici` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL,
  `isim` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL,
  `soyisim` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL,
  `brans` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL,
  `maas` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL,
  `tc` varchar(11) COLLATE utf8_turkish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

/*Data for the table `ogretmen` */

insert  into `ogretmen`(`sifre`,`kulanici`,`isim`,`soyisim`,`brans`,`maas`,`tc`) values ('123','a1','ahmet','doðan','matematik','400','123');

/*Table structure for table `yetkili` */

DROP TABLE IF EXISTS `yetkili`;

CREATE TABLE `yetkili` (
  `kulanici` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL,
  `sifre` varchar(15) COLLATE utf8_turkish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

/*Data for the table `yetkili` */

insert  into `yetkili`(`kulanici`,`sifre`) values ('a1','123');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
